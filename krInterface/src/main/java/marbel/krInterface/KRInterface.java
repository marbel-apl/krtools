/**
 * Knowledge Representation Tools. Copyright (C) 2014 Koen Hindriks.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

package marbel.krInterface;

import java.io.Reader;
import java.util.List;
import java.util.Map;

import marbel.krInterface.database.Database;
import marbel.krInterface.exceptions.KRDatabaseException;
import marbel.krInterface.exceptions.ParserException;
import marbel.krInterface.language.DatabaseFormula;
import marbel.krInterface.language.Query;
import marbel.krInterface.language.Substitution;
import marbel.krInterface.language.Term;
import marbel.krInterface.language.Var;
import marbel.krInterface.parser.Parser;
import marbel.krInterface.parser.SourceInfo;

/**
 * The knowledge representation (KR) interface.
 *
 * <p>
 * A knowledge representation interface should provide the following services:
 * <ul>
 * <li>initializing the interface with a list of references to enable its use.
 * </li>
 * <li>resetting the interface.</li>
 * <li>creating a database.</li>
 * <li>providing a parser for parsing source (files) represented in the KR
 * language.</li>
 * <li>mapping a map of variables and terms to a substitution.</li>
 * <li>getting the queries from the given set of queries that have not been
 * defined.</li>
 * <li>getting the formulas that have been defined in the set of database
 * formulas but are not used (queried).</li>
 * <li>checking if the expressions of the KR Language support serialization.
 * </li>
 * </ul>
 *
 * Logging from implementors can be done through the "KRLogger", got by using
 * {@link Logger#getLogger(String)}.
 */
public interface KRInterface {
	/**
	 * Creates a new database.
	 *
	 * @param name A human-readable name for the database itself.
	 * @return An empty database that can be modified.
	 * @throws KRDatabaseException If the database could not be created.
	 */
	Database getDatabase(String name) throws KRDatabaseException;

	/**
	 * Returns a parser for this KR language. Only initializes the parser but does
	 * not parse the source yet. See {@link Parser} for methods that parse the
	 * input.
	 *
	 * @param source A reader of the source that is to be parsed.
	 * @param info   the {@link SourceInfo}. This is needed as this parser will be
	 *               used as subparser, and then it needs to be able to create
	 *               correct source references and error messages with correct line
	 *               numbers.
	 * @throws ParserException If anything went wrong during initialization of the
	 *                         parser, e.g., due to a problem with the source.
	 */
	Parser getParser(Reader source, SourceInfo info) throws ParserException;

	/**
	 * Creates a substitution from a map of variables to terms.
	 *
	 * @param map The Var to Term mapping; possibly null.
	 *
	 * @return A substitution which binds all variables in the map to the associated
	 *         terms.
	 */
	Substitution getSubstitution(Map<Var, Term> map);

	/**
	 * Reports the queries in the given set of queries that have not been defined in
	 * the set of database formulas as well as those queries that are implicitly
	 * queries in the set of database formulas itself (e.g., as condition in a
	 * rule).
	 */
	List<Query> getUndefined(List<DatabaseFormula> dbfs, List<Query> queries);

	/**
	 * @param dbfs    the defined database formulas
	 * @param queries the queries that are actually done
	 * @return the formulas that have been defined in the set of database formulas
	 *         but are not used (queried).
	 */
	List<DatabaseFormula> getUnused(List<DatabaseFormula> dbfs, List<Query> queries);

	/**
	 * Resets the KR interface. Should clear and free all memory used by the
	 * inference engine and any of the databases that have been created.
	 *
	 * @throws KRDatabaseException If releasing resources was not successful or
	 *                             incomplete.
	 */
	void release() throws KRDatabaseException;
}
