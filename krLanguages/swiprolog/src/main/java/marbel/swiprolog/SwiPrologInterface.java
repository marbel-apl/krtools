/**
 * Knowledge Representation Tools. Copyright (C) 2014 Koen Hindriks.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

package marbel.swiprolog;

import java.io.IOException;
import java.io.Reader;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.jpl7.fli.Prolog;

import marbel.krInterface.KRInterface;
import marbel.krInterface.database.Database;
import marbel.krInterface.exceptions.KRDatabaseException;
import marbel.krInterface.exceptions.ParserException;
import marbel.krInterface.language.DatabaseFormula;
import marbel.krInterface.language.Query;
import marbel.krInterface.language.Substitution;
import marbel.krInterface.language.Term;
import marbel.krInterface.language.Var;
import marbel.krInterface.parser.Parser;
import marbel.krInterface.parser.SourceInfo;
import marbel.swiprolog.database.PrologDatabase;
import marbel.swiprolog.language.PrologSubstitution;
import marbel.swiprolog.parser.Analyzer;
import marbel.swiprolog.parser.KRInterfaceParser4;

/**
 * Implementation of {@link KRInterface} for SWI Prolog.
 */
public final class SwiPrologInterface implements KRInterface {
	/**
	 * Contains all databases that are maintained
	 */
	private final Map<String, PrologDatabase> databases = new ConcurrentHashMap<>();

	/**
	 * See {@link SwiInstaller#init(boolean)}.
	 */
	public SwiPrologInterface() {
		try {
			SwiInstaller.init(false);
		} catch (final Throwable retry) {
			SwiInstaller.init(true);
		}
	}

	@Override
	public Database getDatabase(final String name) throws KRDatabaseException {
		PrologDatabase database = this.databases.get(name);
		if (database == null) {
			Prolog.create_engine();
			database = new PrologDatabase(this, name);
			this.databases.put(name, database);
		}

		return database;
	}

	public void removeDatabase(final PrologDatabase db) throws KRDatabaseException {
		final PrologDatabase removed = this.databases.remove(db.getName());
		if (removed != null) {
			removed.destroy();
			Prolog.destroy_engine();
		}
	}

	@Override
	public Parser getParser(final Reader r, final SourceInfo info) throws ParserException {
		try {
			return new KRInterfaceParser4(r, info);
		} catch (final IOException e) {
			throw new ParserException("failed to parse the reader data as SWI Prolog.", info, e);
		}
	}

	@Override
	public void release() throws KRDatabaseException {
		for (final PrologDatabase db : this.databases.values()) {
			db.destroy();
		}
		this.databases.clear();
	}

	@Override
	public Substitution getSubstitution(final Map<Var, Term> map) {
		final PrologSubstitution substitution = new PrologSubstitution();
		if (map != null) {
			for (final Var var : map.keySet()) {
				substitution.addBinding(var, map.get(var));
			}
		}
		return substitution;
	}

	@Override
	public List<Query> getUndefined(final List<DatabaseFormula> dbfs, final List<Query> queries) {
		final Analyzer analyzer = new Analyzer(dbfs, queries);
		analyzer.analyze();
		return analyzer.getUndefined();
	}

	@Override
	public List<DatabaseFormula> getUnused(final List<DatabaseFormula> dbfs, final List<Query> queries) {
		final Analyzer analyzer = new Analyzer(dbfs, queries);
		analyzer.analyze();
		return analyzer.getUnused();
	}
}