/**
 * Knowledge Representation Tools. Copyright (C) 2014 Koen Hindriks.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

package marbel.swiprolog.language.impl;

import java.util.ArrayList;
import java.util.List;

import marbel.krInterface.language.Substitution;
import marbel.krInterface.language.Term;
import marbel.krInterface.language.Var;
import marbel.krInterface.parser.SourceInfo;
import marbel.swiprolog.language.PrologTerm;

/**
 * A Prolog integer.
 */
class PrologIntImpl extends org.jpl7.Integer implements PrologTerm {
	/**
	 * Information about the source used to construct this integer.
	 */
	private final SourceInfo info;

	/**
	 * Creates an integer (actually a long).
	 *
	 * @param value The integer value.
	 * @param info  A source info object.
	 */
	PrologIntImpl(final long value, final SourceInfo info) {
		super(value);
		this.info = info;
	}

	@Override
	public SourceInfo getSourceInfo() {
		return this.info;
	}

	@Override
	public boolean isClosed() {
		return true;
	}

	@Override
	public boolean isQuery() {
		return false;
	}

	@Override
	public boolean isNumeric() {
		return true;
	}

	@Override
	public List<Var> getFreeVar() {
		return new ArrayList<>(0);
	}

	@Override
	public String getSignature() {
		return this.value + "/0";
	}

	@Override
	public Term applySubst(final Substitution s) {
		return this;
	}

	@Override
	public int hashCode() {
		return Long.hashCode(this.value);
	}

	@Override
	public Substitution unify(final Term term, final Substitution substitution) {
		return equals(term) ? substitution : null;
	}

	@Override
	public String toString() {
		return String.valueOf(this.value);
	}
}