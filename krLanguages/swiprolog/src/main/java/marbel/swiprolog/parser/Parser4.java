package marbel.swiprolog.parser;

import java.io.IOException;
import java.io.Reader;
import java.util.BitSet;
import java.util.SortedSet;
import java.util.TreeSet;

import org.antlr.v4.runtime.ANTLRErrorListener;
import org.antlr.v4.runtime.BailErrorStrategy;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonToken;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.Parser;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.Recognizer;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.atn.ATNConfigSet;
import org.antlr.v4.runtime.atn.PredictionMode;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.ParseCancellationException;

import marbel.krInterface.exceptions.ParserException;
import marbel.krInterface.parser.SourceInfo;
import marbel.swiprolog.errors.ParserErrorMessages;
import swiprolog.parser.Prolog4Lexer;
import swiprolog.parser.Prolog4Parser;
import swiprolog.parser.Prolog4Parser.ListtermContext;
import swiprolog.parser.Prolog4Parser.PossiblyEmptyConjunctContext;
import swiprolog.parser.Prolog4Parser.PossiblyEmptyDisjunctContext;
import swiprolog.parser.Prolog4Parser.PrologtextContext;
import swiprolog.parser.Prolog4Parser.Term0Context;
import swiprolog.parser.Prolog4Parser.Term1000Context;
import swiprolog.parser.Prolog4Parser.Term1150Context;

/**
 * {@link Prolog4Parser} but stores all errors coming from {@link ANTLR} so that
 * we can later report them. All (unchecked) {@link RecognitionException}s are
 * shielded (actually, never thrown directly from the {@link Prolog4Parser}) and
 * changed to {@link ParserException}s.Also it re-throws if there was an error
 * after parsing. This is needed because {@link Prolog4Parser} does not throw
 * when errors occur, instead it "recovers" and never reports us so. <br>
 * This parser therefore checks the results and re-throws the first exception so
 * that we can handle problems with the normal throw/catch mechanisms higher up.
 */
public class Parser4 implements ANTLRErrorListener {
	private final Prolog4Parser parser;
	private final SortedSet<ParserException> errors = new TreeSet<>();
	private final SourceInfo sourceInfo;
	private final CharStream stream;
	private final Lexer lexer;
	private final CommonTokenStream tokens;

	/**
	 * Constructor. Adjusts the tokeniser input stream position matching the given
	 * start position
	 *
	 * @param reader the input text stream to use for parsing.
	 * @param info   The start position (line number, column etc) for this parse.
	 *               Used if the text received really is part of a bigger file. If
	 *               set to null, we use a default info object starting at line 1
	 *               with a file reference set to null.
	 * @throws IOException
	 */
	public Parser4(final Reader reader, final SourceInfo info) throws IOException {
		if (info == null) {
			this.sourceInfo = new SourceInfoObject(null, 1, 1, 0, 0);
		} else {
			this.sourceInfo = info;
		}
		final String name = (this.sourceInfo.getSource() == null) ? "" : this.sourceInfo.getSource();
		this.stream = CharStreams.fromReader(reader, name);

		this.lexer = new Prolog4Lexer(this.stream);
		this.lexer.removeErrorListeners();
		this.lexer.addErrorListener(this);
		this.lexer.setLine(this.sourceInfo.getLineNumber());
		this.lexer.setCharPositionInLine(this.sourceInfo.getCharacterPosition() + 1);

		this.tokens = new CommonTokenStream(this.lexer);
		this.parser = new Prolog4Parser(this.tokens);
		// First try with simpler/faster SLL(*)
		this.parser.getInterpreter().setPredictionMode(PredictionMode.SLL);
		// We don't want error messages or recovery during first try
		this.parser.removeErrorListeners();
		this.parser.setErrorHandler(new BailErrorStrategy());
	}

	/**
	 * @return the initial source info field of this parser.
	 *
	 */
	public SourceInfo getSourceInfo() {
		return this.sourceInfo;
	}

	/**
	 * Check if we processed the whole stream we were given
	 */
	private void checkEndOfInputReached() {
		if (this.stream.index() < this.stream.size()) {
			final SourceInfoObject info = new SourceInfoObject(this.sourceInfo.getSource(), this.lexer.getLine(),
					this.lexer.getCharPositionInLine(), this.sourceInfo.getStartIndex() + this.stream.index() + 1,
					this.sourceInfo.getStartIndex() + this.stream.size() + 1);
			this.errors.add(new ParserException("Unrecognized spurious input", info));
		}
	}

	/**
	 * Get the errors that occurred during parsing.
	 *
	 * @return error set.
	 */
	public SortedSet<ParserException> getErrors() {
		return this.errors;
	}

	/**
	 * Check if parse was a success. To be called after parsing.
	 *
	 *
	 * @return true if parsing was a success = no errors.
	 */
	public boolean isSuccess() {
		return this.errors.isEmpty();
	}

	/**
	 * Renders string from a parse tree result.
	 *
	 * @param tree {@link ParserRuleContext} to render
	 * @return String representation of tree.
	 */
	public String toStringTree(final ParserRuleContext tree) {
		return tree.toStringTree(this.parser);
	}

	/*************** Implements {@link ANTLRErrorListener} *******************/
	@Override
	public void syntaxError(final Recognizer<?, ?> recognizer, final Object offendingSymbol, final int line,
			final int charPositionInLine, final String msg, final RecognitionException e) {
		int start = recognizer.getInputStream().index();
		int stop = start;
		if (offendingSymbol != null) {
			final CommonToken token = (CommonToken) offendingSymbol;
			start = token.getStartIndex();
			stop = token.getStopIndex();
		}
		final SourceInfoObject pos = new SourceInfoObject(this.sourceInfo.getSource(), line, charPositionInLine,
				this.sourceInfo.getStartIndex() + start + 1, this.sourceInfo.getStartIndex() + stop + 1);
		if (recognizer instanceof Lexer) {
			handleLexerError(recognizer, offendingSymbol, pos, msg, e);
		} else {
			handleParserError(recognizer, offendingSymbol, pos, msg, e);
		}
	}

	/**
	 * Adds new error for token recognition problem (lexer).
	 *
	 * @param pos  input stream position
	 * @param stop stopIndex of last recognition error
	 * @param text character(s) that could not be recognized
	 */
	public void handleLexerError(final Recognizer<?, ?> recognizer, final Object offendingSymbol,
			final SourceInfoObject pos, String text, final RecognitionException e) {
		text = text.replace("\\r", "").replace("\\n", " ").replace("\\t", " ").replace("\\f", "");
		this.errors.add(new ParserException(ParserErrorMessages.CANNOT_BE_USED.toReadableString(text), pos));
	}

	/**
	 * Adds error for parsing problem.
	 *
	 * <p>
	 * Simply pushes parser error msg forward. See {@link #MASErrorStrateg} for
	 * handling of parsing errors.
	 * </p>
	 *
	 * @param pos input stream position
	 * @param msg reported parser error msg
	 */
	public void handleParserError(final Recognizer<?, ?> recognizer, final Object offendingSymbol,
			final SourceInfoObject pos, final String expectedtokens, final RecognitionException e) {
		// We need the strategy to get access to our customized token displays
		final ErrorStrategy4 strategy = (ErrorStrategy4) ((Parser) recognizer).getErrorHandler();
		// Report the various types of syntax errors
		final String offendingTokenText = strategy.getTokenErrorDisplay((Token) offendingSymbol);
		// TODO: copies and hardcodes derived from LanguageTools->Validator
		if (e.getMessage().equals("NoViableAlternative") || e.getMessage().equals("InputMismatch")) {
			this.errors.add(new ParserException(
					ParserErrorMessages.FOUND_BUT_NEED.toReadableString(offendingTokenText, expectedtokens), pos));
		} else if (e.getMessage().equals("FailedPredicate")) {
			this.errors.add(new ParserException(ParserErrorMessages.FAILED_PREDICATE.toReadableString(), pos));
		} else if (e.getMessage().equals("UnwantedToken")) {
			this.errors
					.add(new ParserException(ParserErrorMessages.TOKEN_BAD.toReadableString(offendingTokenText), pos));
		} else if (e.getMessage().equals("MissingToken")) {
			this.errors
					.add(new ParserException(ParserErrorMessages.TOKEN_MISSING.toReadableString(expectedtokens), pos));
		} else {
			this.errors.add(new ParserException(
					ParserErrorMessages.EXPECTED_TEXT.toReadableString(offendingTokenText, expectedtokens), pos));
		}
	}

	@Override
	public void reportAmbiguity(final Parser recognizer, final DFA dfa, final int startIndex, final int stopIndex,
			final boolean exact, final BitSet ambigAlts, final ATNConfigSet configs) {
	}

	@Override
	public void reportAttemptingFullContext(final Parser recognizer, final DFA dfa, final int startIndex,
			final int stopIndex, final BitSet conflictingAlts, final ATNConfigSet configs) {
	}

	@Override
	public void reportContextSensitivity(final Parser recognizer, final DFA dfa, final int startIndex,
			final int stopIndex, final int prediction, final ATNConfigSet configs) {
	}

	public void switchToFullLL() {
		// First rewind the token stream
		this.tokens.seek(0);
		// Use full (custom) error reporting now
		this.parser.setErrorHandler(new ErrorStrategy4());
		this.parser.addErrorListener(this);
		// Now try full LL(*)
		this.parser.getInterpreter().setPredictionMode(PredictionMode.LL);
	}

	/************** Actual public Parser functionality *******************/
	public Term0Context term0() {
		Term0Context t;
		try {
			t = this.parser.term0();
		} catch (final ParseCancellationException e) {
			switchToFullLL();
			t = this.parser.term0();
		}
		checkEndOfInputReached();
		return t;
	}

	public PossiblyEmptyConjunctContext possiblyEmptyConjunct() {
		PossiblyEmptyConjunctContext t;
		try {
			t = this.parser.possiblyEmptyConjunct();
		} catch (final ParseCancellationException e) {
			switchToFullLL();
			t = this.parser.possiblyEmptyConjunct();
		}
		checkEndOfInputReached();
		return t;
	}

	public PrologtextContext prologtext() {
		PrologtextContext t;
		try {
			t = this.parser.prologtext();
		} catch (final ParseCancellationException e) {
			switchToFullLL();
			t = this.parser.prologtext();
		}
		checkEndOfInputReached();
		return t;
	}

	public PossiblyEmptyDisjunctContext possiblyEmptyDisjunct() {
		PossiblyEmptyDisjunctContext t;
		try {
			t = this.parser.possiblyEmptyDisjunct();
		} catch (final ParseCancellationException e) {
			switchToFullLL();
			t = this.parser.possiblyEmptyDisjunct();
		}
		checkEndOfInputReached();
		return t;
	}

	public Term1000Context term1000() {
		Term1000Context t;
		try {
			t = this.parser.term1000();
		} catch (final ParseCancellationException e) {
			switchToFullLL();
			t = this.parser.term1000();
		}
		checkEndOfInputReached();
		return t;
	}

	/**
	 * @return parser for term1150. For test purposes.
	 */
	public Term1150Context term1150() {
		Term1150Context t;
		try {
			t = this.parser.term1150();
		} catch (final ParseCancellationException e) {
			switchToFullLL();
			t = this.parser.term1150();
		}
		checkEndOfInputReached();
		return t;
	}

	public ListtermContext listterm() {
		ListtermContext t;
		try {
			t = this.parser.listterm();
		} catch (final ParseCancellationException e) {
			switchToFullLL();
			t = this.parser.listterm();
		}
		checkEndOfInputReached();
		return t;
	}
}