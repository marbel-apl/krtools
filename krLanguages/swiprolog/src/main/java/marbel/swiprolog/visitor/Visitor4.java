package marbel.swiprolog.visitor;

import java.util.ArrayList;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

import marbel.krInterface.exceptions.ParserException;
import marbel.swiprolog.language.PrologTerm;
import marbel.swiprolog.parser.Parser4;
import swiprolog.parser.Prolog4Parser.PossiblyEmptyConjunctContext;
import swiprolog.parser.Prolog4Parser.PossiblyEmptyDisjunctContext;
import swiprolog.parser.Prolog4Parser.PrologtextContext;
import swiprolog.parser.Prolog4Parser.Term0Context;
import swiprolog.parser.Prolog4Parser.Term1000Context;

/**
 * Visitor that converts a parse tree coming from an {@link Parser4} into a
 * {@link PrologTerm}.<br>
 * <h1>example</h1> example parsing a term0<br>
 * * <code>
		marbel.swiprolog.visitor = new Visitor4(
				new ErrorStoringProlog4Parser(new StringReader("term"), null));<br>
		PrologTerm term = marbel.swiprolog.visitor.visitTerm0();<br>
 * </code>
 */
public class Visitor4 {
	private final Parser4 parser;
	private final Visitor4Internal visitor;

	/**
	 * @param parser a {@link Parser4}
	 */
	public Visitor4(final Parser4 p) {
		this.parser = p;
		this.visitor = new Visitor4Internal(p.getSourceInfo());
	}

	public PrologTerm visitPossiblyEmptyConjunct() {
		final PossiblyEmptyConjunctContext parsed = this.parser.possiblyEmptyConjunct();
		return this.parser.isSuccess() ? this.visitor.visitPossiblyEmptyConjunct(parsed) : null;
	}

	public List<PrologTerm> visitPrologtext() {
		final PrologtextContext parsed = this.parser.prologtext();
		return this.parser.isSuccess() ? this.visitor.visitPrologtext(parsed) : new ArrayList<>(0);
	}

	public PrologTerm visitPossiblyEmptyDisjunct() {
		final PossiblyEmptyDisjunctContext parsed = this.parser.possiblyEmptyDisjunct();
		return this.parser.isSuccess() ? this.visitor.visitPossiblyEmptyDisjunct(parsed) : null;
	}

	public PrologTerm visitTerm0() {
		final Term0Context parsed = this.parser.term0();
		return this.parser.isSuccess() ? this.visitor.visitTerm0(parsed) : null;
	}

	public PrologTerm visitTerm1000() {
		final Term1000Context parsed = this.parser.term1000();
		return this.parser.isSuccess() ? this.visitor.visitTerm1000(parsed) : null;
	}

	/**
	 * Get all errors from both marbel.swiprolog.visitor and parser
	 *
	 * @return all errors from both marbel.swiprolog.visitor and parser
	 */
	public SortedSet<ParserException> getErrors() {
		final SortedSet<ParserException> allErrors = new TreeSet<>(this.visitor.getVisitorErrors());
		allErrors.addAll(this.parser.getErrors());
		return allErrors;
	}
}