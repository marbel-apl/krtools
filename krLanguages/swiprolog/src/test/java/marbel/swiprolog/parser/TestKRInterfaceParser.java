package marbel.swiprolog.parser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.io.StringReader;

import org.junit.Test;

import marbel.krInterface.language.Update;
import marbel.krInterface.parser.Parser;
import marbel.krInterface.parser.SourceInfo;
import marbel.swiprolog.SwiPrologInterface;
import marbel.swiprolog.parser.KRInterfaceParser4;
import marbel.swiprolog.parser.SourceInfoObject;

public class TestKRInterfaceParser {
	private static final SourceInfoObject INFO = new SourceInfoObject("test", 1, 1, 1, 1);

	@Test
	public void testParseUpdate() throws Exception {
		new SwiPrologInterface();
		final StringReader reader = new StringReader("on(a,b), on(b,c), on(c,table)");
		final Parser parser = new KRInterfaceParser4(reader, INFO);
		final Update update = parser.parseUpdate();

		assertEquals(",/2", update.getSignature());
		assertEquals("on(a,b) , on(b,c) , on(c,table)", update.toString());
	}

	@Test
	public void testParseUpdate_2() throws Exception {
		new SwiPrologInterface();
		final StringReader reader = new StringReader("zone(ID, Name, X, Y, Neighbours)");
		final Parser parser = new KRInterfaceParser4(reader, INFO);
		final Update update = parser.parseUpdate();

		assertEquals("zone/5", update.getSignature());
		assertEquals("zone(ID,Name,X,Y,Neighbours)", update.toString());
	}

	// @Test FIXME: this fails atm
	public void parseLessEqualError() throws Exception {
		// X <= 3 is not correct because "<=" does not exist in prolog.
		new SwiPrologInterface();
		final StringReader reader = new StringReader("X <= 3");
		final Parser parser = new KRInterfaceParser4(reader, INFO);
		parser.parseQuery();
		for (final SourceInfo e : parser.getErrors()) {
			System.out.println(e);
			assertFalse(e.toString().contains("basic term 'nothing'"));
		}
	}
}
