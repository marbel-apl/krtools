/**
 * Knowledge Representation Tools. Copyright (C) 2014 Koen Hindriks.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

package marbel.swiprolog.language;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import marbel.krInterface.language.Substitution;
import marbel.krInterface.language.Term;
import marbel.krInterface.language.Var;
import marbel.swiprolog.language.PrologCompound;
import marbel.swiprolog.language.PrologSubstitution;
import marbel.swiprolog.language.impl.PrologImplFactory;

public class TestPrologSubstitution {
	@Test
	public void testSubstitution() {
		final Substitution solution = new PrologSubstitution();
		final Var X = PrologImplFactory.getVar("X", null);
		final Var Y = PrologImplFactory.getVar("Y", null);
		final Var Z = PrologImplFactory.getVar("Z", null);
		solution.addBinding(X, Y);
		solution.addBinding(Y, Z);

		final Term term = PrologImplFactory.getCompound("aap", new Term[] { X, Y }, null);

		final Term result = term.applySubst(solution);
		assertTrue(result instanceof PrologCompound);
		final PrologCompound compound = (PrologCompound) result;
		assertEquals(Y, compound.getArg(0));
		assertEquals(Z, compound.getArg(1));
	}

	@Test
	public void testToString() {
		final Substitution solution = new PrologSubstitution();
		assertTrue(solution.getVariables().isEmpty());

		final Var X = PrologImplFactory.getVar("X", null);
		final Term a = PrologImplFactory.getAtom("a", null);
		solution.addBinding(X, a);
		assertEquals(1, solution.getVariables().size());
		assertEquals(a, solution.get(X));

		final Var Y = PrologImplFactory.getVar("Y", null);
		final Term b = PrologImplFactory.getAtom("b", null);
		solution.addBinding(Y, b);
		assertEquals(2, solution.getVariables().size());
		assertEquals(a, solution.get(X));
		assertEquals(b, solution.get(Y));

		final Var Z = PrologImplFactory.getVar("Z", null);
		final Term V = PrologImplFactory.getVar("V", null);
		solution.addBinding(Z, V);
		assertEquals(3, solution.getVariables().size());
		assertEquals(a, solution.get(X));
		assertEquals(b, solution.get(Y));
		assertEquals(V, solution.get(Z));
	}
}
