/**
 * Knowledge Representation Tools. Copyright (C) 2014 Koen Hindriks.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

package marbel.swiprolog.language;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import marbel.krInterface.language.Substitution;
import marbel.krInterface.language.Term;
import marbel.krInterface.language.Var;
import marbel.swiprolog.language.PrologSubstitution;
import marbel.swiprolog.language.impl.PrologImplFactory;

public class TestUnification {
	/**
	 * Test case: unification of basic constants.
	 */
	@Test
	public void test1unify() throws Exception {
		final Term a = PrologImplFactory.getAtom("a", null);
		final Term b = PrologImplFactory.getAtom("b", null);

		assertEquals(null, a.mgu(b));
	}

	/**
	 * Test case: unification of single variable X with constant a.
	 */
	@Test
	public void test2unify() {
		final Var x = PrologImplFactory.getVar("X", null);
		final Term a = PrologImplFactory.getAtom("a", null);

		final Substitution unifier = new PrologSubstitution(x, a);
		assertEquals(unifier, x.mgu(a));
	}

	/**
	 * Test case: unification of variable X with term f(...) where ... does not
	 * contain X.
	 */
	@Test
	public void test3unify() {
		// f(a)
		final Var X = PrologImplFactory.getVar("X", null);
		final Term a = PrologImplFactory.getAtom("a", null);
		final Term fa = PrologImplFactory.getCompound("f", new Term[] { a }, null);

		final Substitution unifier1 = new PrologSubstitution(X, fa);
		assertEquals(unifier1, X.mgu(fa));
		assertEquals(unifier1, fa.mgu(X));

		// f(a, b)
		final Term b = PrologImplFactory.getAtom("b", null);
		final Term fab = PrologImplFactory.getCompound("f", new Term[] { a, b }, null);

		final Substitution unifier2 = new PrologSubstitution(X, fab);
		assertEquals(unifier2, X.mgu(fab));
		assertEquals(unifier2, fab.mgu(X));

		// f(Y, b)
		final Var Y = PrologImplFactory.getVar("Y", null);
		final Term fYb = PrologImplFactory.getCompound("f", new Term[] { Y, b }, null);

		final Substitution unifier3 = new PrologSubstitution(X, fYb);
		assertEquals(unifier3, X.mgu(fYb));
		assertEquals(unifier3, fYb.mgu(X));
	}

	/**
	 * Test case: unification of f(X,Y) and f(a,b) and f(a,b,c).
	 */
	@Test
	public void test4unify() {
		// Construct f(X, Y)
		final Var X = PrologImplFactory.getVar("X", null);
		final Var Y = PrologImplFactory.getVar("Y", null);
		final Term fXY = PrologImplFactory.getCompound("f", new Term[] { X, Y }, null);
		// Construct f(a, b)
		final Term a = PrologImplFactory.getAtom("a", null);
		final Term b = PrologImplFactory.getAtom("b", null);
		final Term fab = PrologImplFactory.getCompound("f", new Term[] { a, b }, null);
		// Construct f(a, b, c)
		final Term c = PrologImplFactory.getAtom("c", null);
		final Term fabc = PrologImplFactory.getCompound("f", new Term[] { a, b, c }, null);

		final Substitution unifier = new PrologSubstitution(X, a);
		unifier.addBinding(Y, b);

		assertEquals(unifier, fXY.mgu(fab));
		assertEquals(unifier, fab.mgu(fXY));
		assertEquals(null, fXY.mgu(fabc));
		assertEquals(null, fabc.mgu(fXY));
	}

	/**
	 * Test case: unification of f(a, X) and f(Y, Z).
	 */
	@Test
	public void test5unify() {
		// Construct f(a, X)
		final Var X = PrologImplFactory.getVar("X", null);
		final Term a = PrologImplFactory.getAtom("a", null);
		final Term faX = PrologImplFactory.getCompound("f", new Term[] { a, X }, null);
		// Construct f(Y, Z)
		final Var Y = PrologImplFactory.getVar("Y", null);
		final Var Z = PrologImplFactory.getVar("Z", null);
		final Term fYZ = PrologImplFactory.getCompound("f", new Term[] { Y, Z }, null);

		final Substitution unifier1 = new PrologSubstitution(Y, a);
		unifier1.addBinding(X, Z);
		assertEquals(unifier1, faX.mgu(fYZ));

		final Substitution unifier2 = new PrologSubstitution(Y, a);
		unifier2.addBinding(Z, X);
		assertEquals(unifier2, fYZ.mgu(faX));
	}

	/**
	 * Test case: unification of f(a, X) and f(X, a).
	 */
	@Test
	public void test6unify() {
		// Construct f(a, X)
		final Var X = PrologImplFactory.getVar("X", null);
		final Term a = PrologImplFactory.getAtom("a", null);
		final Term faX = PrologImplFactory.getCompound("f", new Term[] { a, X }, null);
		// Construct f(X, a)
		final Term fXa = PrologImplFactory.getCompound("f", new Term[] { X, a }, null);

		final Substitution unifier = new PrologSubstitution(X, a);
		assertEquals(unifier, faX.mgu(fXa));
		assertEquals(unifier, fXa.mgu(faX));
	}

	/**
	 * Test case: unification of f(X, X) and f(a, b).
	 */
	@Test
	public void test7unify() {
		// Construct f(X, X)
		final Var X = PrologImplFactory.getVar("X", null);
		final Term fXX = PrologImplFactory.getCompound("f", new Term[] { X, X }, null);
		// Construct f(a, b)
		final Term a = PrologImplFactory.getAtom("a", null);
		final Term b = PrologImplFactory.getAtom("b", null);
		final Term fab = PrologImplFactory.getCompound("f", new Term[] { a, b }, null);

		assertEquals(null, fXX.mgu(fab));
		assertEquals(null, fab.mgu(fXX));
	}

	/**
	 * Test case: unification of f(X, X) and f(a, Y).
	 */
	@Test
	public void test8unify() {
		// Construct f(X, X)
		final Var X = PrologImplFactory.getVar("X", null);
		final Term fXX = PrologImplFactory.getCompound("f", new Term[] { X, X }, null);
		// Construct f(a, b)
		final Term a = PrologImplFactory.getAtom("a", null);
		final Var Y = PrologImplFactory.getVar("Y", null);
		final Term faY = PrologImplFactory.getCompound("f", new Term[] { a, Y }, null);

		final Substitution unifier = new PrologSubstitution(X, a);
		unifier.addBinding(Y, a);

		assertEquals(unifier, fXX.mgu(faY));
		assertEquals(unifier, faY.mgu(fXX));
	}

	/**
	 * Test case: f(g(Y), X, Y) = f(X, g(a), a).
	 */
	@Test
	public void test9unify() {
		// Construct f(g(Y), X, Y)
		final Var X = PrologImplFactory.getVar("X", null);
		final Var Y = PrologImplFactory.getVar("Y", null);
		final Term gY = PrologImplFactory.getCompound("g", new Term[] { Y }, null);
		final Term fgYXY = PrologImplFactory.getCompound("f", new Term[] { gY, X, Y }, null);
		// Construct f(X, g(a), a)
		final Term a = PrologImplFactory.getAtom("a", null);
		final Term gA = PrologImplFactory.getCompound("g", new Term[] { a }, null);
		final Term fXgaa = PrologImplFactory.getCompound("f", new Term[] { X, gA, a }, null);
		// Construct f(g(Y), X)
		final Term fgYX = PrologImplFactory.getCompound("f", new Term[] { gY, X }, null);
		// Construct f(X, g(a))
		final Term fXga = PrologImplFactory.getCompound("f", new Term[] { X, gA }, null);

		final Substitution unifier1 = new PrologSubstitution(X, gY);
		final Substitution unifier2 = new PrologSubstitution(X, gY);
		unifier2.addBinding(Y, a);

		assertEquals(unifier1, gY.mgu(X));
		assertEquals(unifier2, fgYX.mgu(fXga));
		// f(g(Y), X, Y) = f(X, g(a), a)
		assertEquals(unifier2, fgYXY.mgu(fXgaa));
	}

	/**
	 * Test case: unification of f(X, Y) and f(Y, X).
	 */
	@Test
	public void test10unify() {
		// Construct f(X, Y)
		final Var X = PrologImplFactory.getVar("X", null);
		final Var Y = PrologImplFactory.getVar("Y", null);
		final Term fXY = PrologImplFactory.getCompound("f", new Term[] { X, Y }, null);
		// Construct f(Y, X)
		final Term fYX = PrologImplFactory.getCompound("f", new Term[] { Y, X }, null);

		final Substitution unifier1 = new PrologSubstitution(X, Y);
		final Substitution unifier2 = new PrologSubstitution(Y, X);

		assertEquals(unifier1, fXY.mgu(fYX));
		assertEquals(unifier2, fYX.mgu(fXY));
	}

	/**
	 * Test case: unification of f(X) and f(g(X)) (occurs check should kick in).
	 * #3470
	 */
	@Test
	public void test11unify() {
		// Construct f()
		final Var X = PrologImplFactory.getVar("X", null);
		final Term fX = PrologImplFactory.getCompound("f", new Term[] { X }, null);
		// Construct f(g(X))
		final Term gX = PrologImplFactory.getCompound("g", new Term[] { X }, null);
		final Term fgX = PrologImplFactory.getCompound("f", new Term[] { gX }, null);

		assertEquals(null, fX.mgu(fgX));
		assertEquals(null, fgX.mgu(fX));
	}

	/**
	 * Test case: X should match with X. #3469
	 */
	@Test
	public void test12unify() {
		final Var X = PrologImplFactory.getVar("X", null);

		assertEquals(new PrologSubstitution(), X.mgu(X));
	}

	/**
	 * Test case: unification of f(X,X) and f(X,X)
	 */
	@Test
	public void test13unify() {
		// Construct f(X, X)
		final Var X = PrologImplFactory.getVar("X", null);
		final Term fXX = PrologImplFactory.getCompound("f", new Term[] { X, X }, null);

		assertEquals(new PrologSubstitution(), fXX.mgu(fXX));
	}

	@Test
	public void testOccursCheck() {
		// Construct aap(X)
		final Var X = PrologImplFactory.getVar("X", null);
		final Term aapX = PrologImplFactory.getCompound("aap", new Term[] { X }, null);

		assertEquals(null, aapX.mgu(X));
		assertEquals(null, X.mgu(aapX));
	}

	/**
	 * Test case: aap(1,X) versus aap(2,X)
	 */
	@Test
	public void test14unify() {
		// Construct aap(1,X)
		final Var X = PrologImplFactory.getVar("X", null);
		final Term one = PrologImplFactory.getAtom("1", null);
		final Term aap1X = PrologImplFactory.getCompound("aap", new Term[] { one, X }, null);
		// Construct aap(2,X)
		final Term two = PrologImplFactory.getAtom("2", null);
		final Term aap2X = PrologImplFactory.getCompound("aap", new Term[] { two, X }, null);

		assertEquals(null, aap1X.mgu(aap2X));
		assertEquals(null, aap2X.mgu(aap1X));
	}

	/**
	 * Test case: aap(1,beer(3)) versus aap(2,beer(3))
	 */
	@Test
	public void test15unify() {
		// Construct beer(3)
		final Term three = PrologImplFactory.getAtom("3", null);
		final Term beer3 = PrologImplFactory.getCompound("beer", new Term[] { three }, null);
		// Construct aap(1,beer(3))
		final Term one = PrologImplFactory.getAtom("1", null);
		final Term aap1beer3 = PrologImplFactory.getCompound("aap", new Term[] { one, beer3 }, null);
		// Construct aap(2,beer(3))
		final Term two = PrologImplFactory.getAtom("2", null);
		final Term aap2beer3 = PrologImplFactory.getCompound("aap", new Term[] { two, beer3 }, null);

		assertEquals(null, aap1beer3.mgu(aap2beer3));
		assertEquals(null, aap2beer3.mgu(aap1beer3));
	}

	/**
	 * Test case: '1' versus 1
	 */
	@Test
	public void testAtom1Int1() {
		final Term oneI = PrologImplFactory.getNumber(1, null);
		final Term oneA = PrologImplFactory.getAtom("1", null);

		assertEquals(null, oneI.mgu(oneA));
		assertEquals(null, oneA.mgu(oneI));
	}
}
