package marbel.swiprolog.database;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import marbel.krInterface.KRInterface;
import marbel.krInterface.database.Database;
import marbel.krInterface.exceptions.KRDatabaseException;
import marbel.swiprolog.SwiPrologInterface;
import marbel.swiprolog.language.PrologCompound;
import marbel.swiprolog.language.impl.PrologImplFactory;

public class TestUpdate2 {
	// components enabling us to run the tests...
	private KRInterface language;
	private Database db;
	// basic knowledge
	private PrologCompound k1;
	private PrologCompound k2;
	private PrologCompound k3;

	@Before
	public void setUp() throws Exception {
		this.language = new SwiPrologInterface();
		this.k1 = PrologImplFactory.getAtom("aap", null);
		this.k2 = PrologImplFactory.getAtom("beer", null);
		this.k3 = PrologImplFactory.getAtom("kat", null);
		fillKB();
	}

	@After
	public void tearDown() throws Exception {
		if (this.db != null) {
			this.db.destroy();
		}
	}

	/**
	 * Make standard kb and bb where KB holds two knowledge items
	 *
	 * @throws KRDatabaseException
	 */
	private void fillKB() throws KRDatabaseException {
		final String agentName = TestUpdate2.class.getSimpleName();
		this.db = this.language.getDatabase(agentName);
		this.db.insert(PrologImplFactory.getDBFormula(this.k1));
		this.db.insert(PrologImplFactory.getDBFormula(this.k2));
	}

	/**
	 * alternative fill for KB.
	 *
	 * @throws KRDatabaseException
	 */
	private void fillKB2() throws KRDatabaseException {
		final String agentName = TestUpdate2.class.getSimpleName();
		this.db = this.language.getDatabase(agentName);
		this.db.insert(PrologImplFactory.getDBFormula(this.k3));
	}

	/**
	 * Delete all databases. Just a smoke test, as we can't do anything with deleted
	 * databases.
	 */
	@Test
	public void testDeleteAll() throws Exception {
		this.db.destroy();
	}

	/**
	 * Create new Kb and Bb for SAME AGENT NAME, and check that the new BB has the
	 * new Kb content.
	 */
	@Test
	public void testRecreateKbAndBb() throws Exception {
		this.db.destroy();
		// ok, now we can recreate the KB. But this time different.
		fillKB2();
	}
}
