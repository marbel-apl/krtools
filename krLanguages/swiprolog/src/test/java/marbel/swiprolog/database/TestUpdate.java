package marbel.swiprolog.database;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import marbel.krInterface.KRInterface;
import marbel.krInterface.database.Database;
import marbel.krInterface.exceptions.KRDatabaseException;
import marbel.krInterface.exceptions.KRQueryFailedException;
import marbel.krInterface.language.DatabaseFormula;
import marbel.krInterface.language.Substitution;
import marbel.swiprolog.SwiPrologInterface;
import marbel.swiprolog.language.PrologCompound;
import marbel.swiprolog.language.PrologQuery;
import marbel.swiprolog.language.impl.PrologImplFactory;

public class TestUpdate {
	// components enabling us to run the tests...
	private KRInterface language;
	private Database db;
	// beliefs
	private PrologCompound aap;
	private PrologCompound kat;

	@Before
	public void setUp() throws Exception {
		this.language = new SwiPrologInterface();
		final String agentName = TestUpdate.class.getSimpleName();
		this.db = this.language.getDatabase(agentName);
		this.aap = PrologImplFactory.getAtom("aap", null);
		this.kat = PrologImplFactory.getAtom("kat", null);
	}

	@After
	public void tearDown() throws Exception {
		if (this.db != null) {
			this.db.destroy();
		}
	}

	@Test
	public void testInitialQuery1() throws Exception {
		final PrologQuery query = PrologImplFactory.getQuery(PrologImplFactory.getAtom("true", null));
		final List<Substitution> sol = this.db.query(query);
		assertEquals(1, sol.size());
	}

	/**
	 * Check that inserting 'aap' results in 'aap' query to become true and that
	 * there is 1 sentence in beliefbase after the insert.
	 */
	@Test
	public void testInsertFormula() throws Exception {
		final DatabaseFormula formula = PrologImplFactory.getDBFormula(this.aap);
		this.db.insert(formula);

		final PrologQuery query = PrologImplFactory.getQuery(this.aap);
		final List<Substitution> sol = this.db.query(query);
		assertEquals(1, sol.size());
	}

	/**
	 * After creation of new (empty) BB, check that the new BB also works by
	 * inserting something and checking that it gets there.
	 */
	@Test
	public void testUseNewBeliefbase() throws KRQueryFailedException, KRDatabaseException {
		final DatabaseFormula formula = PrologImplFactory.getDBFormula(this.kat);
		this.db.insert(formula);

		final PrologQuery query = PrologImplFactory.getQuery(this.kat);
		final List<Substitution> sol = this.db.query(query);
		assertEquals(1, sol.size());
	}
}
