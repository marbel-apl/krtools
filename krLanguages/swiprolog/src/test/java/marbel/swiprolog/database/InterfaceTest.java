package marbel.swiprolog.database;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import marbel.krInterface.database.Database;
import marbel.krInterface.exceptions.KRDatabaseException;
import marbel.krInterface.language.DatabaseFormula;
import marbel.swiprolog.SwiPrologInterface;
import marbel.swiprolog.language.PrologCompound;
import marbel.swiprolog.language.impl.PrologImplFactory;

public class InterfaceTest {
	private SwiPrologInterface swi;
	// we're being lazy, maybe we should mock these?
	private final PrologCompound a = PrologImplFactory.getAtom("a", null);
	private final DatabaseFormula aformula = PrologImplFactory.getDBFormula(this.a);

	@Before
	public void before() {
		this.swi = new SwiPrologInterface();
	}

	@After
	public void after() throws KRDatabaseException {
		this.swi.release();
	}

	@Test
	public void testCache() throws KRDatabaseException {
		assertEquals(this.aformula, this.aformula);

		final String agentName = InterfaceTest.class.getSimpleName();
		final Database db1 = this.swi.getDatabase(agentName);
		db1.insert(this.aformula);
		final Database db2 = this.swi.getDatabase(agentName);

		assertEquals(db1, db2);
	}
}
